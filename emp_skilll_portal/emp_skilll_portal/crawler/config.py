import psycopg2

def db_connection():
    """
    Establish DB connection
    """
    conn = psycopg2.connect("dbname='empskillsdb' user='postgres' host='localhost' password='Manager@123'")
    cur = conn.cursor()
    conn.commit()
    return cur, conn
