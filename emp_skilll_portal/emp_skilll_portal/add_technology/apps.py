from django.apps import AppConfig


class AddTechnologyConfig(AppConfig):
    name = 'add_technology'
