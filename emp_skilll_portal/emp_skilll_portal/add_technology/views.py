from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from webapp.models import Employee,Technology,EmpTechnology


@csrf_exempt
def addtech(request):
   data_list = []
   if request.method == 'POST':
       technology = Technology()
       technology.technology = request.POST.get('technology',None)
       technology.digital = request.POST.get('digital',None)
       list = request.POST.getlist('apptype', None)
       print(request.POST.getlist('apptype', None))
       technology.apptype = list
       for item in list:
           print(item)
           technology.apptype = item
       technology.category = request.POST.get('category',None)
       technology.save()
   else:

       get_technology = Technology.objects.filter(status=True).order_by('id')
       if get_technology:
           for doc in get_technology:
               # print(doc.technology)

               value_dict = {}
               value_dict['id'] = doc.id
               value_dict['technology'] = doc.technology
               value_dict['digital'] = doc.digital
               value_dict['apptype'] = doc.apptype
               value_dict['app_final'] = doc.app_final

               if value_dict['id']:
                   data_list.append(value_dict)
   print (data_list)
   return render(request, 'add_technology/add.html', {'result_entries': data_list})





