# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2019-04-27 04:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('add_technology', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emptechnology',
            name='empid',
        ),
        migrations.RemoveField(
            model_name='emptechnology',
            name='tech_id',
        ),
        migrations.DeleteModel(
            name='Employee',
        ),
        migrations.DeleteModel(
            name='EmpTechnology',
        ),
        migrations.DeleteModel(
            name='Technology',
        ),
    ]
