from django.conf.urls import url
from webapp import views
from .views import index, crawler_setting, get_salary_details,get_profile_details
urlpatterns = [
    url(r'^$', index, name='dashboard-index'),
    url(r'^salary_details/$', get_salary_details, name='get-salary-details'),
    url(r'^crawler_setting/$', crawler_setting, name='crawler-setting'),
    url(r'^profile/$', get_profile_details, name='profile'),


]
