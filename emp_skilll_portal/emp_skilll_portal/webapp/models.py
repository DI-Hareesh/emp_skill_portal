from django.db import models


class Employee(models.Model):
    """
    Model for store Employee details
    """
    ibs_empid= models.CharField(max_length=20,null=True, blank=True)
    emp_name = models.CharField(max_length=200,null=True, blank=True)
    designation = models.CharField(max_length=200,null=True, blank=True)
    about = models.CharField(max_length=100000,null=True, blank=True)
    contact = models.CharField(max_length=100000,null=True, blank=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.ibs_empid


class Technology(models.Model):
    """
        Model for store Technology
    """
    technology = models.CharField(max_length=200, null=True, blank=True)
    digital = models.CharField(max_length=200, null=True, blank=True)
    apptype = models.CharField(max_length=200, null=True, blank=True)
    category =  models.CharField(max_length=200, null=True, blank=True)
    status = models.BooleanField(default=True)
    app_final= models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.technology

class EmpTechnology(models.Model):
    """
        Model for store emp_technology
    """
    empid = models.ForeignKey(Employee, null=True, blank=True, on_delete=True)
    tech_id = models.ForeignKey(Technology, null=True, blank=True,on_delete=True)

    def __str__(self):
        return self.technology

