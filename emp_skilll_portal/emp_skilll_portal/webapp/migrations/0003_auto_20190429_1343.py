# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2019-04-29 08:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0002_auto_20190427_0937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='about',
            field=models.CharField(blank=True, max_length=10000, null=True),
        ),
    ]
