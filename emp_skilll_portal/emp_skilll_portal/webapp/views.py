from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from webapp.models import Employee,Technology,EmpTechnology
from django.http import JsonResponse
from django.db.models import Count
from django.db.models import Sum
from collections import OrderedDict
import psycopg2

from django.db.models import Q
from functools import reduce

@csrf_exempt
def index(request):


   return render(request, 'webapp/index.html')




def get_profile_details(request):
    emp_id= request.GET.get('id', None)
    e_id=Employee.objects.get(ibs_empid__icontains= emp_id)
    dict={}
    d={}
    final =[]
    id=e_id.id
    name=e_id.emp_name
    contact=e_id.contact
    dict["name"]= name
    emp_tech_id = EmpTechnology.objects.filter(empid_id= id)
    tag_lists = []
    children_list =[]
    for tid_objs in emp_tech_id:
        get_tech_items = Technology.objects.filter(id=tid_objs.tech_id_id).distinct('app_final').values_list('app_final')
        for items in get_tech_items:
            tag_lists.extend(items)
        tag_sets=list(OrderedDict.fromkeys(tag_lists).keys())
    for tag_set in tag_sets:
            c = {
                "name": tag_set,
                "children": [
                ]
            }
            for tid_obj in emp_tech_id:
                get_items = Technology.objects.filter(id=tid_obj.tech_id_id,app_final=tag_set).distinct('app_final').values_list('technology')
                for get_item in get_items:
                     d =  { "name":get_item[0], "value": 415 }
                     c["children"].append(d)
            children_list.append(c)
            dict['children'] = children_list
    final.append(dict)
    return render(request, 'webapp/profile.html', {"context": final})




@csrf_exempt
def get_salary_details(request):
    if request.POST:
        technology =''
        technology_name = request.POST.getlist('technology_name', None)

        name = technology_name[0].split(',')
        cnt = 0
        emp_details_list = []
        tech_details_list=[]
        for names in name:
            tech_objs = Technology.objects.filter(Q(technology__icontains=names.strip()) | Q(apptype__icontains=names.strip()))

            context = []
            for tech_obj in tech_objs:

                id_objs=EmpTechnology.objects.filter(tech_id=tech_obj.id)
                for id_obj  in id_objs:

                    emp_details= Employee.objects.filter(id=id_obj.empid_id)
                    emp_details_list.extend(emp_details)
                    #for eid in emp_details:

                        #tid=EmpTechnology.objects.filter(empid_id=eid.id)
                        #for tid_objs in tid:

                            #tech_details=Technology.objects.filter(id=tid_objs.tech_id_id)

                            #for tech in tech_details:
                                #total_digital=Technology.objects.filter(digital__icontains='Yes').count()
                                #print(total_digital,"...............")
                                #digitals= Technology.objects.filter(id=tid_objs.tech_id_id,digital__icontains='No').count()




                                # #digital=Technology.objects.filter(id=tid_objs.tech_id_id).filter(digital__icontains='No').count()
                                #
                                # #print(tech.digital,tech.technology,"............")
                                #
                                #
                                # tech_detail=tech.technology
                                # #print(tech_detail)
                                #
                                #
                                # tech_details_list.extend(tech_detail)
                                # #print(tech_details_list,"..........................................")






        rendered = render_to_string('webapp/view_searchresult.html', {"emp": emp_details_list,"tech":tech_details_list})
        return JsonResponse({"html":rendered}, safe=False)



@csrf_exempt
def crawler_setting(request):
    """
          page for run and create new crawler
    """
    active_crawlers =''
    # active_crawlers = Company.objects.filter(state=True).order_by('created_date')
    return render(request, 'webapp/crawler_setting.html',{'contexts': active_crawlers})

