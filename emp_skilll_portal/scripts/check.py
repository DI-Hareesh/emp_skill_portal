import psycopg2
import re
import os, time
import glob
import docx
from docx import Document
import datetime

conn = psycopg2.connect("dbname='empskillsdb' user='postgres' host='localhost' password='Manager@123'")
cur = conn.cursor()
path_to_watch = r"D:\employee_skill_portal_Backup\resume"
before = dict([(f, None) for f in os.listdir(path_to_watch)])

for name in glob.glob(f'resume/*.docx'):
    try:
        eid = re.findall(r"[A-Z][-][0-9]+", name)
        eid = str(eid).strip('[](),''\'')
        if (len(eid)> 0) :
            select_query = """SELECT status FROM  webapp_employee where ibs_empid='%s'"""
            select_query = select_query % eid
            cur.execute(select_query)
            status = cur.fetchall()
            status = str(status).strip('[](),''\'')
            print("**sssss****", status, type(status), len(status))
            print(name,"Inside the GLOB Function")
            doc = Document(name)
            resume_name = name
            flag = 0
            text_list = []
            doc = Document(name)
            name = doc.paragraphs[0].text
            print(name)
            role = doc.paragraphs[1].text
            phone = doc.paragraphs[2].text
            blank_line_count = 0
            for p in doc.paragraphs:
                if p.text == '':
                    blank_line_count += 1
                    flag = 0
                else:
                    flag = 1
                if blank_line_count > 1:
                    text_list.append(p.text)
                if blank_line_count > 2 and flag == 0:
                    break

            skills_details = [x for x in text_list if x]
            print(type(skills_details))
            skills_details= str(skills_details).strip('[](),''\'')
            print(skills_details)
            insert_query = "INSERT INTO webapp_employee(ibs_empid,emp_name,designation,about,contact,status)VALUES(%s,%s,%s,%s,%s,%s)"
            record_insert = (eid, name,role,skills_details, phone,True)
            cur.execute(insert_query, record_insert)
            conn.commit()
            tech1 = {}

            for row in doc.tables[0].rows:
                if (row.cells[0].text == "Technology"):
                    continue
                if (row.cells[0].text == ""):
                    continue
                else:
                    print("..............", row.cells[0].text)

                    tech1.update({row.cells[0].text: row.cells[1].text})
            items = []
            print(tech1)  ####resume technology details dictionary
            for insert_tech in tech1:
                tech_details = tech1[insert_tech].split(",")
                for item in tech_details:
                    print(item)
                    trimmed = re.sub(r"^\s+", "", item)
                    items.append(trimmed)
            for data in items:
                data=data.strip()
                tech_query = "select id from webapp_technology where lower(technology) like lower('%s')"
                tech_query = tech_query % data
                cur.execute(tech_query)
                techid = cur.fetchall()
                techid=list(techid)
                conn.commit()
                select_id = "select id from webapp_employee where ibs_empid='%s'"
                select_id = select_id % eid
                cur.execute(select_id)
                employee_id = cur.fetchall()
                print(f'employee id {employee_id}')
                employee_id=str(employee_id).strip('{}[](),''\'')
                conn.commit()
                for id in techid:
                    insert = "INSERT INTO webapp_emptechnology(empid_id,tech_id_id)VALUES(%s,%s)"
                    record = (employee_id, id)
                    cur.execute(insert, record)
                    conn.commit()

    except (Exception, psycopg2.Error) as error:
        pass
        print("Error while connecting to PostgreSQL", error)

conn.close()
cur.close()